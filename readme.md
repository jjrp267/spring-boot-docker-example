## Spring Boot docker

en el maven:

### Build 

clean install dockerfile:build


### run

startup:

docker run -p 8080:8080 -t springio/gs-spring-boot-docker


### stop:

docker ps

CONTAINER ID        IMAGE                            COMMAND                  CREATED             STATUS              PORTS                    NAMES
bf521ef512de        springio/gs-spring-boot-docker   "java -cp app:app/li…"   9 minutes ago       Up 9 minutes        0.0.0.0:8080->8080/tcp   quizzical_jang


docker stop bf521ef512de

### delete container

docker rm bf521ef512de


